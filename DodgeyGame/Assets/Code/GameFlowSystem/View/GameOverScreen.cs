﻿using Core.Extensions;
using GooglePlayGames;
using ScoreSystem;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace GameFlowSystem.View
{
	public class GameOverScreen : MonoBehaviour
	{
		#region Serialize Fields

		[SerializeField] private Button _replayButton;
		[SerializeField] private TextMeshProUGUI _scoreText;
		[SerializeField] private TextMeshProUGUI _highScoreText;

		#endregion

		#region Private Fields

		private CanvasGroup _canvasGroup;

		#endregion

		#region Unity methods

		private void Awake()
		{
			_canvasGroup = GetComponent<CanvasGroup>();
			_replayButton.onClick.AddListener(OnClickReplay);
			Hide();

			// unlock achievement (achievement ID "CgkI_bHXhp0TEAIQCA")
			Social.ReportProgress("CgkI_bHXhp0TEAIQCA", 100.0f, (success) =>
			{
				// handle success or failure
			});
		}

		private void OnDestroy()
		{
			_replayButton.onClick.AddListener(OnClickReplay);
		}

		#endregion

		#region Public methods

		public void SetScore(int score)
		{
			_scoreText.text = $"Score: {score.ToString()}";
			Social.ReportScore(ScoreManager.Instance.HighScore,
			GPGSIds.leaderboard_global,
			
			(res) =>
			{
				//PlayGamesPlatform.Instance.ShowLeaderboardUI();
				// handle score post result if desired
				
			});
			//Debug.Log("Testing Debug");
		}

		public void Show()
		{
			_canvasGroup.Enable();
		}

		private void Hide()
		{
			_canvasGroup.Disable();
		}

		#endregion

		#region Private methods

		private void OnClickReplay()
		{
			GameFlow.Instance.GameOverState.Continue = true;
		}

		#endregion
	}
}